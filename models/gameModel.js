var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

let gameSchema = new Schema({
    name: String,
    genre: String, 
    console_name: String,
    console_id: ObjectId,
   
})


module.exports = mongoose.model('game', gameSchema, 'game');