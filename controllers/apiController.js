var mongoose = require('mongoose'),
    videoGame = mongoose.model('videoGame')
    game = mongoose.model('game');

exports.listVideoGame = function(req, res){
    videoGame.find({}, function(err, result){
        if (err){
            res.send(err)
        }
        res.json({
            sucess: result? true: false,
            data: result
        });
    });
}

exports.createVideoGame = function(req, res){
    let newVideoGame = new videoGame(req.body);
    newVideoGame.save(function(err, result){
        if(err){
            res.send(err);
        }
        res.json(result);
    });
}

exports.listGame = function(req, res){
    game.find({},function(err, result){
        if(err){
            res.send(err);
        }
        res.json({
            sucess: result? true: false,
            data: result
        });
    });
}

exports.createGame = function(req, res){
    newGame = new game(req.body);
    videoGame.findOne({name: req.body.console_name})
        .then(setId => newGame.console_id = setId._id)
        .then(
            () => newGame.save()
                .then(result => res
                    .json({
                        sucess: result? true: false,
                        data: result
                    }))
        )
        .catch(err => res.send(err))

    //     , function(err, setId){
    //     newGame.console_id = setId._id;
    //     newGame.save(function(err, result){
    //         if(err){
    //             res.send(err);
    //         }
    //         res.send(result);
    //     });
    // });

   
}

exports.listJogosPerVideoGame = function(req, res){
    game.find({console_name: req.params.console_name}, function(err, result){
        if(err){
            res.send(err);
        }
        res.json({
            sucess: result? true: false,
            data: result
        });
    });
}

exports.findGameById = function(req, res){
    game.findById(req.params._id, function(err, result){
        if(err){
            res.send(err)
        }
        res.json({
            sucess: result? true: false,
            data: result
        });
    });
}

exports.deleteGameById =  function(req, res){
    game.findByIdAndDelete(req.params._id, function(err,result){
        if(err){
            res.send(err);
        }
        res.json({
            sucess: result? true: false,
            data: result
        });
    });
}

exports.findVideoGameById = function(req, res){
    videoGame.findById(req.params._id, function(err, result){
        if(err){
            res.send(err)
        }
        res.json({
            sucess: result? true: false,
            data: result
        });
    });
}

exports.deleteVideoGameById = function(req, res){
    videoGame.findByIdAndDelete(req.params._id, function(err, result){
        if(err){
            res.send(err);
        }
        res.json({
            sucess: result? true: false,
            data: result
        });
    });
}