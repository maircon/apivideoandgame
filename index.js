var express = require('express'),
    app = express(),
    bodyParse = require('body-parser'),
    mongoose = require('mongoose'),
    db = 'mongodb://teste:teste123@ds121135.mlab.com:21135/videoandgame',
    routes = require('./routes/apiRoute'),
    videoGame = require('./models/videoGameModel'),
    game = require('./models/gameModel'),
    port = process.env.PORT || 3000;

app.use(bodyParse.urlencoded({ extended: true }));
app.use(bodyParse.json());

mongoose.connect(db,{ useNewUrlParser: true })
    .then(()=> console.log("Conexao com o banco realizada com sucesso:"))
    .catch((err) => console.log(err));

app.listen(port,()=>{
    console.log("Esperando na porta: "+port);
})

routes(app);

