module.exports = function(app){
    var apiController = require('../controllers/apiController');
    
    app.route('/videogame')
        .get(apiController.listVideoGame)
        .post(apiController.createVideoGame);

    app.route('/game')
        .get(apiController.listGame)
        .post(apiController.createGame);
        
    app.route('/list/gameConsole/:console_name')
        .get(apiController.listJogosPerVideoGame);

    app.route('/game/:_id')
        .get(apiController.findGameById)
        .delete(apiController.deleteGameById)
        
    app.route('/videoGame/:_id')
        .get(apiController.findVideoGameById)
        .delete(apiController.deleteVideoGameById)

}