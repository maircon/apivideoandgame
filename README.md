   
   **Simples API de video games e jogos**

   **O BANCO POSSUI ALGUNS JOGOS E VIDEOGAMES**
   
   Listar video games cadastrados => **GET /videogame**
   
   Criar novo video game => **POST /videogame**
   
   Encontrar video game por id => **GET videogame/:_id**
   
   Deletar video game por id => **DELETE videogame/:_id**
   
   Listar jogos cadastrados => **GET /game**
   
   Criar novo jogo => **POST /game**
   
   Encontrar jogo por id => **GET game/:_id**
   
   Deletar jogo por id => **DELETE game/:_id**
   
   Listar jogos por video games => **GET /list/gameConsole/:console_name**
   
   
   `Modelos de requisição`
   
   
   **Corpo do modelo Video game:**
   
   {
       
       "name": String
       
       "company": String
       
       "nickname": String
       
   }
   
   **Corpo do modelo Game:**
   
   {
        
        "name": String
        
        "genre": String
        
        "console_name": String
        
        
   }