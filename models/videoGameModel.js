var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let videoGameSchema = new Schema({
    name: String,
    company: String,
    nick_name: String
})


module.exports = mongoose.model('videoGame', videoGameSchema, 'videogame');